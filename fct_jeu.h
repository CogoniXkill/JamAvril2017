#ifndef FC
#define FC

#define TMAX 100
#define NB 50
#define TAILLE 7

typedef struct aiguilles {
    int dir1;
    int dir2;
    int longueur;
    int pos[2];
}horloge;

typedef struct plateforme_mouvantes {
    int pos[2];
    int dir[2];
    int longueur;
    int posmin[2];
    int posmax[2];
}plateforme;

typedef struct personnage_t
{
    int x;
    int y;
    int fx;
    int fy;
    int nf;
    int ausol;

} personnage;

typedef struct niveau_t
{
    int num ;
    int matrice_niv[TMAX][TMAX] ;
    int fond_niv[TMAX][TMAX] ;
    horloge * liste_hor[NB] ;
    int nb_horloge ;
    plateforme * liste_platforme[NB] ;
    int nb_plateforme ;
    int posini[2] ;

} niveau;


void afficher (personnage * perso,niveau * niv,SDL_Window * window);
void init(personnage * perso,niveau * niv);
void rotation (horloge * ph , int sens);
void depPlateforme(plateforme * pPlat, int sens);



void initniv(niveau * pNiv, int niv, personnage * pPer );


#endif
