
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "menu.h"
#include "fct_jeu.h"






int valider(SDL_Window * window, int * quit)
{
    /**variable boucle SDL*/
    SDL_Event event ;
    int continuer = 1;
    int valide = 0;
    const Uint8 *state = SDL_GetKeyboardState(NULL);

    /**variable dessin */
    SDL_Surface * image = NULL;
    SDL_Rect rect;

    /**dessin demarage*/

    image=IMG_Load("./data/valider.jpg");
    if(!image) {
        printf("IMG_Load: %s\n", IMG_GetError());
    }
    rect.x = 0;
    rect.y = 0;
    rect.w = 800;
    rect.h = 500;
    if ( SDL_BlitSurface(image,NULL,SDL_GetWindowSurface(window),&rect) != 0 )
    {
        fprintf(stderr,"Erreur de copie de la surface sur l'�cran\n");
    }
    SDL_UpdateWindowSurface(window);
    while(continuer)
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT || state[SDL_SCANCODE_ESCAPE]) {
                continuer = 0;
                *quit = 1;
            }
            if (state[SDL_SCANCODE_Y]) {
                valide=1;
                continuer = 0;
            }
            if (state[SDL_SCANCODE_X])  {
                valide=0;
                continuer = 0;
            }

            if  (event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    int x = event.button.x;
                    int y = event.button.y;
                    if((y<300)&&(y>200))
                    {
                        if((x<280)&&(x>130))
                        {
                            valide=1;
                            continuer = 0;
                        }
                        if((x<670)&&(x>520))
                        {
                            valide=0;
                            continuer = 0;
                        }
                    }
                }
            }
        }
        SDL_Delay(50);
    }

    SDL_FreeSurface(image);
    return(valider);

}

void ecrandemarage(SDL_Window * window,int *fonction,int *quit)
{
    /**variable boucle SDL*/
    SDL_Event event ;
    int continuer = 1;
    int choix = 0;
    const Uint8 *state = SDL_GetKeyboardState(NULL);

    /**variable dessin */
    SDL_Surface * image = NULL;
    SDL_Rect rect;

    /**variable teste*/



    /**dessin demarage*/
    image=IMG_Load("./data/ecrandemarage.jpg");
    if(!image) {
        printf("IMG_Load: %s\n", IMG_GetError());
    }
    rect.x = 0;
    rect.y = 0;
    rect.w = 800;
    rect.h = 500;
    if ( SDL_BlitSurface(image,NULL,SDL_GetWindowSurface(window),&rect) != 0 )
    {
        fprintf(stderr,"Erreur de copie de la surface sur l'�cran\n");
    }
    SDL_UpdateWindowSurface(window);

    /*---------------------------*/
    /*-  a remplacer par % de   -*/
    /*-  de la sauve            -*/
    /*---------------------------*/

    /**boucle principale*/

    while(continuer)
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT || state[SDL_SCANCODE_ESCAPE]) {
                continuer = 0;
                *quit = 1;
            }
            if ((state[SDL_SCANCODE_1])||(state[SDL_SCANCODE_KP_1])) {
                choix=1;
                continuer = 0;
            }
            if ((state[SDL_SCANCODE_2])||(state[SDL_SCANCODE_KP_2]))  {
                choix=2;
                continuer = 0;
            }
            if ((state[SDL_SCANCODE_3])||(state[SDL_SCANCODE_KP_3]))  {
                choix=3;
                continuer = 0;
            }
            if ((state[SDL_SCANCODE_4])||(state[SDL_SCANCODE_KP_4]))  {
                choix=4;
                continuer = 0;
            }
            if (state[SDL_SCANCODE_DELETE]) {
                choix=-1;
                continuer = 0;
            }
            if  (event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    int x = event.button.x;
                    int y = event.button.y;
                    if((y<300)&&(y>150))
                    {
                        if((x<190)&&(x>40))
                        {
                            choix=1;
                            continuer = 0;
                        }
                        if((x<380)&&(x>230))
                        {
                            choix=2;
                            continuer = 0;
                        }
                        if((x<570)&&(x>420))
                        {
                            choix=3;
                            continuer = 0;
                        }
                        if((x<760)&&(x>610))
                        {
                            choix=4;
                            continuer = 0;
                        }
                    }
                    if((y<430)&&(y>370))
                    {
                        if((x<380)&&(x>40))
                        {
                            choix=-1;
                            continuer = 0;
                        }
                    }
                }
            }
        }
        SDL_Delay(50);
    }
    if (choix>0)
    {
        /*initsauv(choix);*/
        *fonction = 2;
    }
    if (choix<0)
    {
        *fonction = 1;
    }

    SDL_FreeSurface(image);
}




void ecransuppr(SDL_Window * window,int *fonction,int *quit)
{
    /**variable boucle SDL*/
    SDL_Event event ;
    int continuer = 1;
    int choix = 0;
    const Uint8 *state = SDL_GetKeyboardState(NULL);

    /**variable dessin */
    SDL_Surface * image = NULL;
    SDL_Rect rect;

    /**variable teste*/

    TTF_Font* pFont = NULL;
    const char* fontPath = "./data/font.ttf";
    SDL_Color fontColor = {255, 255, 255};
    SDL_Surface* pFontSurface = NULL;
    SDL_Rect texteDestination;

    char texte[100] ;

    /**dessin suppression*/

    image=IMG_Load("./data/ecransuppr.jpg");
    if(!image) {
        printf("IMG_Load: %s\n", IMG_GetError());
    }
    rect.x = 0;
    rect.y = 0;
    rect.w = 800;
    rect.h = 500;
    if ( SDL_BlitSurface(image,NULL,SDL_GetWindowSurface(window),&rect) != 0 )
    {
        fprintf(stderr,"Erreur de copie de la surface sur l'�cran\n");
    }
    SDL_UpdateWindowSurface(window);

    /*---------------------------*/
    /*-  a remplacer par % de   -*/
    /*-  de la sauve            -*/
    /*---------------------------*/


    /**boucle principale*/

    while(continuer)
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT || state[SDL_SCANCODE_ESCAPE]) {
                continuer = 0;
                *quit = 1;
            }
            if ((state[SDL_SCANCODE_1])||(state[SDL_SCANCODE_KP_1])) {
                choix=1;
                continuer = 0;
            }
            if ((state[SDL_SCANCODE_2])||(state[SDL_SCANCODE_KP_2]))  {
                choix=2;
                continuer = 0;
            }
            if ((state[SDL_SCANCODE_3])||(state[SDL_SCANCODE_KP_3]))  {
                choix=3;
                continuer = 0;
            }
            if ((state[SDL_SCANCODE_4])||(state[SDL_SCANCODE_KP_4]))  {
                choix=4;
                continuer = 0;
            }
            if (state[SDL_SCANCODE_BACKSPACE]) {
                choix=-1;
                continuer = 0;
            }
            if  (event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    int x = event.button.x;
                    int y = event.button.y;
                    if((y<300)&&(y>150))
                    {
                        if((x<190)&&(x>40))
                        {
                            choix=1;
                            continuer = 0;
                        }
                        if((x<380)&&(x>230))
                        {
                            choix=2;
                            continuer = 0;
                        }
                        if((x<570)&&(x>420))
                        {
                            choix=3;
                            continuer = 0;
                        }
                        if((x<760)&&(x>610))
                        {
                            choix=4;
                            continuer = 0;
                        }
                    }
                    if((y<430)&&(y>370))
                    {
                        if((x<380)&&(x>40))
                        {
                            choix = -1;
                            continuer = 0;
                        }
                    }
                }
            }
        }
        SDL_Delay(50);
    }
    if (choix>0)
    {
        if(valider(window,quit))
        {
            /*supprsauv(choix);*/
            *fonction = 0;
        }

    }
    if (choix<0)
    {
        *fonction = 0;
    }

    SDL_FreeSurface(pFontSurface);
    TTF_CloseFont(pFont);
    SDL_FreeSurface(image);
}



void ecranfin(SDL_Window * window,int *fonction,int *quit)
{
    /**variable boucle SDL*/
    SDL_Event event ;
    int continuer = 1;
    int choix = 0;
    const Uint8 *state = SDL_GetKeyboardState(NULL);

    /**variable dessin */
    SDL_Surface * image = NULL;
    SDL_Rect rect;

    /**variable teste*/

    TTF_Font* pFont = NULL;
    const char* fontPath = "./data/font.ttf";
    SDL_Color fontColor = {255, 255, 255};
    SDL_Surface* pFontSurface = NULL;
    SDL_Rect texteDestination;

    char texte[100] ;

    /**dessin fin*/

    image=SDL_LoadBMP("ecranfin.bmp");
    if(!image) {
        printf("IMG_Load: %s\n", IMG_GetError());
    }
    rect.x = 0;
    rect.y = 0;
    rect.w = 800;
    rect.h = 500;
    if ( SDL_BlitSurface(image,NULL,SDL_GetWindowSurface(window),&rect) != 0 )
    {
        fprintf(stderr,"Erreur de copie de la surface sur l'�cran\n");
    }
    SDL_UpdateWindowSurface(window);



    /**boucle principale*/

    while(continuer)
    {
        while(SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT || state[SDL_SCANCODE_ESCAPE]) {
                continuer = 0;
                *quit = 1;
            }
            if ((state[SDL_SCANCODE_1])||(state[SDL_SCANCODE_KP_1])) {
                choix=1;
                continuer = 0;
            }
            if ((state[SDL_SCANCODE_2])||(state[SDL_SCANCODE_KP_2]))  {
                choix=2;
                continuer = 0;
            }
            if  (event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (event.button.button == SDL_BUTTON_LEFT)
                {
                    int x = event.button.x;
                    int y = event.button.y;
                    if((y<460)&&(y>360))
                    {
                        if((x<280)&&(x>130))
                        {
                            choix=1;
                            continuer = 0;
                        }
                        if((x<670)&&(x>520))
                        {
                            choix=2;
                            continuer = 0;
                        }
                    }
                }
            }
        }
        SDL_Delay(50);
    }
    if (choix>0)
    {
        if (choix==1)
        {
            *fonction = 2;
        }
        if (choix==2)
        {
            *fonction = 0;
        }

    }


    SDL_FreeSurface(pFontSurface);
    TTF_CloseFont(pFont);
    SDL_FreeSurface(image);

}




void ecranjeu(SDL_Window * window,int * afonction,int * aquit)
{
    /** variable SDL */
    int continuer = 1; /* Indicateur boolean pour la boucle principale */
    int continuniv = 1;
    int ftot[2] = {0,0};
    SDL_Event event ;
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    int left =0;
    int up =0;
    int right =0;
    int down =0;
    int k=0;
    int avtmp=0;
    int rectmp=0;
    int tmprotat =1;
    /** variable d'initialisation */
    personnage * perso = (personnage*)malloc(sizeof(personnage));
    niveau * niv = (niveau*)malloc(sizeof(niveau)) ;
    int numniv ;

    int l =0;
    int deplacement = 1;
    /** boucle principale */
    if ((perso != NULL)&&(niv != NULL))
    {
        initniv(niv,1,perso);
        h_vers_mat(niv);
        p_vers_mat(niv);
        if ( window != NULL )
        {
            while (continuer) /** on rentre dans un nouveau niveau */
            {
                afficher(perso,niv,window);
                SDL_UpdateWindowSurface(window);
                while ((continuer)&&(continuniv)) /** on est dans le niveau */
                {
                    int sol = 0;
                    int trouver=0;
                    for(sol=0;sol<5;sol++)
                    {
                        if(niv->matrice_niv[perso->y+1][perso->x-2+sol] != 0)
                        {
                            perso->ausol = 1 ;
                            trouver = 1;
                            if(perso->fy > 0)
                            {
                                perso->fy = 0;
                            }
                        }
                    }
                    if(trouver==0)
                    {
                        perso->ausol = 0;
                    }


                    while(SDL_PollEvent(&event))
                    {/* Si oui, quel type? */
                        switch (event.type)
                        {
                              /* Appui sur une touche */
                          case SDL_KEYDOWN:
                               switch (event.key.keysym.sym)
                               {
                               case SDLK_ESCAPE:
                                continuer = 0;
                                *aquit = 1;
                               case SDLK_LEFT:
                                left = 1;
                                break;
                               case SDLK_RIGHT:
                                right = 1;
                                break;
                               case SDLK_UP:
                                up = -1;
                                break;
                               case SDLK_DOWN:
                                down = 1;
                                break;
                               case SDLK_a:
                                rectmp = 1;
                                break;
                               case SDLK_e:
                                avtmp = 1;
                                break;
                               default:
                                break;
                               }
                               break;
                              /* Rel�chement d'une touche */
                          case SDL_KEYUP:
                               switch (event.key.keysym.sym)
                               {
                               case SDLK_LEFT:
                                left =0;
                                break;
                               case SDLK_RIGHT:
                                right =0;
                                break;
                               case SDLK_UP:
                                up =0;
                                break;
                               case SDLK_DOWN:
                                down = 0;
                                break;
                               case SDLK_a:
                                rectmp = 0;
                                tmprotat = 1;
                                break;
                               case SDLK_e:
                                avtmp = 0;
                                tmprotat = 1;
                                break;
                               default:
                                break;
                               }
                               break;
                        }
                    }
                    if (event.type == SDL_QUIT) {
                        continuer = 0;
                        *aquit = 1;
                    }
                    if (up) {
                        if(perso->ausol)
                        {
                            perso->fx = perso->fx + 0;
                            perso->fy = perso->fy - 20;
                        }

                    }

                    if (left)  {
                        if(perso->ausol)
                        {
                            perso->fx = perso->fx - 5;
                            perso->fy = perso->fy + 0;
                        }
                        else
                        {
                            perso->fx = perso->fx - 5;
                            perso->fy = perso->fy + 0;
                        }
                    }
                    if (right)  {
                        if(perso->ausol)
                        {
                            perso->fx = perso->fx + 5;
                            perso->fy = perso->fy + 0;
                        }
                        else
                        {
                            perso->fx = perso->fx + 5;
                            perso->fy = perso->fy + 0;
                        }
                    }
                    if (down)  {
                        if(perso->ausol == 0)
                        {
                            perso->fx = perso->fx + 0;
                            perso->fy = perso->fy + 2;
                        }
                    }
                    if ((avtmp)&&(tmprotat)) {
                        int h;
                        mat_vers_p(niv);
                        mat_vers_h(niv);
                        for(h=0;h<niv->nb_horloge;h++)
                        {
                            rotation(niv->liste_hor[h],1);
                        }
                        for(h=0;h<niv->nb_plateforme;h++)
                        {
                            depPlateforme(niv->liste_platforme[h],1);
                        }
                        h_vers_mat(niv);
                        p_vers_mat(niv);
                        tmprotat = 0;
                    }

                    if ((rectmp)&&(tmprotat)) {

                        int h;
                        mat_vers_p(niv);
                        mat_vers_h(niv);
                        for(h=0;h<niv->nb_horloge;h++)
                        {
                            rotation(niv->liste_hor[h],-1);
                        }
                        for(h=0;h<niv->nb_plateforme;h++)
                        {
                            depPlateforme(niv->liste_platforme[h],-1);
                        }
                        h_vers_mat(niv);
                        p_vers_mat(niv);
                        tmprotat = 0;
                    }


                    perso->fx = (perso->fx)*0.5 ;
                    perso->fy = (perso->fy)*0.7+2 ;

                    if(perso->fx<-40)
                    {
                        perso->fx = -40 ;
                    }
                    if(perso->fy<-40)
                    {
                        perso->fy = -40 ;
                    }
                    if(perso->fx>40)
                    {
                        perso->fx = 40 ;
                    }
                    if(perso->fy>40)
                    {
                        perso->fy = 40 ;
                    }


                    /**deplacement avec collision*/
                    deplacement = 1;
                    if(perso->fy>0)
                    {
                        k=0;
                        while(k<abs(perso->fy)&&deplacement)
                        {

                            for(l=0;l<5;l++)
                            {
                                if(niv->matrice_niv[perso->y+k+1][perso->x-2+l] != 0)
                                {
                                    perso->ausol = 1 ;
                                    if(perso->fy > 0)
                                    {
                                        perso->fy = 0;
                                    }
                                    deplacement = 0;
                                }
                            }
                            k++;
                        }
                        perso->y = perso->y +k-1;
                    }
                    deplacement = 1;
                    if(perso->fx>0)
                    {
                        k=0;
                        while((k<abs(perso->fx))&&(deplacement))
                        {

                            for(l=0;l<10;l++)
                            {

                                if(niv->matrice_niv[perso->y-l][perso->x+3+k] != 0)
                                {
                                    /**printf("x=%d ;",perso->x+3);
                                    printf("y=%d ;",perso->y-l);
                                    printf("matric = %d;",niv->matrice_niv[perso->y-l][perso->x+3]);*/
                                    perso->fx = 0;
                                    deplacement = 0;
                                }
                            }
                            k++;
                        }
                        perso->x = perso->x +k-1;
                    }
                    deplacement = 1;
                    if(perso->fx<0)
                    {
                        k=0;
                        while((k<abs(perso->fx))&&(deplacement))
                        {

                            for(l=0;l<10;l++)
                            {

                                if(niv->matrice_niv[perso->y-l][perso->x-3-k] != 0)
                                {
                                    perso->fx = 0;
                                    deplacement = 0;
                                }
                            }
                            k++;
                        }
                        perso->x = perso->x -k+1;
                    }
                    deplacement = 1;
                    if(perso->fy<0)
                    {
                        k=0;
                        while(k<abs(perso->fy)&&deplacement)
                        {

                            for(l=0;l<5;l++)
                            {

                                if(niv->matrice_niv[perso->y-10-k][perso->x-2+l] != 0)
                                {
                                    perso->fy = 0;
                                    deplacement = 0;
                                }
                            }
                            k++;
                        }
                        perso->y = perso->y -k+1;
                    }



                    afficher(perso,niv,window);
                    SDL_UpdateWindowSurface(window);



                    SDL_Delay(50);

                }


                SDL_UpdateWindowSurface(window);
                SDL_Delay(50);


            }
        }
        else
        {
            printf("Erreur de cr�ation de la fen�tre: %s\n",SDL_GetError());
            return -3;
        }
    }
    else
    {
        printf("erreur malloc \n");
    }

    *afonction = 3;
    liberer(niv);
    free(niv);
    free(perso);

}
